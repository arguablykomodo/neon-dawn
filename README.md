# 1212: Neon Dawn

## Blender exporter

Blender exports to Godot via [`godot-blender-exporter`](https://github.com/godotengine/godot-blender-exporter/archive/blender2.8.zip). To install it, download the zip file and move it to blender's addon folder.

### Where is the addon folder?

- On Linux: `~/.config/blender/2.80/scripts/addons`
- On Windows: `%APPDATA%\Blender Foundation\Blender\2.80\scripts\addons`

## Collision layers

- Layer 0: Environment
- Layer 1: Enemies
- Layer 2: Projectiles
- Layer 10: Marano

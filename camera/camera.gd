extends Spatial

onready var marano: KinematicBody = get_tree().get_nodes_in_group("marano")[0];

# Follow Marano
func _process(delta):
	translation = marano.translation;

extends Entity
class_name Boss

onready var marano: KinematicBody = get_tree().get_nodes_in_group("marano")[0];

var revealed = false;

func _process(delta):
	if translation.distance_to(marano.translation) < 24 && !revealed:
		EVENTS.emit_signal("boss_spawned", life, name);
		revealed = true;

func _on_hitbox_body_entered(body):
	._on_hitbox_body_entered(body);
	EVENTS.emit_signal("boss_life_changed", life);
	if life <= 0:
		EVENTS.emit_signal("boss_died");
extends Boss

onready var punch_timer: Timer = $"Punch timer";

var accel := Vector3();
var line_of_sight := false;

func _physics_process(delta):
	look_at(marano.translation, Vector3.UP);
	var direction := (marano.translation - translation).normalized();
	var distance := translation.distance_to(marano.translation);
	
	if distance < 4 && punch_timer.time_left == 0:
		EVENTS.emit_signal("capone_punch", direction);
		punch_timer.start();
	
	accel *= 0.9;
	
	if distance < 24 || line_of_sight:
		line_of_sight = true;
		var prev = translation;
		move_and_slide(direction * 15 + accel);
		var movement = translation - prev;
		if movement.length() < 0.1:
			accel.x += rand_range(-100, 100);
			accel.z += rand_range(-100, 100);
	
func _on_lunge():
	var direction := (marano.translation - translation).normalized();
	accel += direction * 200;

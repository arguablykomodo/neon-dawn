extends Entity

onready var marano: KinematicBody = get_tree().get_nodes_in_group("marano")[0];
onready var gun: Gun = $Gun;
var accel := Vector3(0, 0, 0);
var follow_distance := 32;
var stop_distance := 16;
var thrust := 1.5;
var friction := 0.95;
var max_speed := 15;

func _physics_process(delta):
	var target := marano.translation;
	target.y += 12;
	var pos := transform.origin;
	var direction := (target - pos).normalized();
	var distance := pos.distance_to(target);

	if distance < follow_distance:
		gun.shoot(true);
		if distance > stop_distance:
			accel += direction * thrust;
	
	accel *= friction;
	if accel.x > max_speed: accel.x = max_speed;
	if accel.y > max_speed: accel.y = max_speed;
	if accel.z > max_speed: accel.z = max_speed;

	look_at(target, Vector3.UP);
	rotation.x = 0;
	rotation.y += PI;

	move_and_slide(accel);

extends KinematicBody
class_name Entity

export (int) var life;
export (bool) var friendly;

onready var hitbox: Area = $Hitbox;
onready var explosion := preload("res://entities/explosion.tscn");

func _ready():
	hitbox.connect("body_entered", self, "_on_hitbox_body_entered");

func _on_hitbox_body_entered(body):
	if body is Projectile and body.is_friendly != friendly:
		life -= body.damage;
		body.queue_free();
		if life <= 0:
			var kaboom: Particles = explosion.instance();
			kaboom.translation = translation;
			get_parent().add_child(kaboom);
			queue_free();

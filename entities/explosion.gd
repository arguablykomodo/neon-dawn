extends Spatial

onready var tween: Tween = $Tween;
onready var light: OmniLight = $Light;

func _ready():
	tween.interpolate_property(light, "light_energy", 20, 0, 1, Tween.TRANS_CIRC, Tween.EASE_OUT);
	tween.start();

func _on_time_to_die():
	queue_free();

extends Entity

onready var camera: Camera = get_tree().get_nodes_in_group("camera")[0];
onready var trail: Particles = $Particles
onready var anim: AnimationPlayer = $Mesh/AnimationPlayer;
onready var mr_skeltal: Skeleton = $Mesh/Armature;
onready var gun := $Gun;
onready var viewport := get_tree().get_root();
onready var screen_size := viewport.get_visible_rect().size;

var speed := 30;
var accel := Vector3();

func _ready():
	EVENTS.connect("level_changed", self, "_on_level_changed");
	EVENTS.connect("capone_punch", self, "_on_capone_punch");
	anim.get_animation("Walk_Leftward").set_loop(true);
	anim.get_animation("Walk_Rightward").set_loop(true);
	anim.get_animation("Walk_Forward").set_loop(true);
	anim.set_speed_scale(0.9);
	
	var ev := InputEventAction.new();
	ev.action = "right";
	ev.pressed = true;
	Input.parse_input_event(ev);

func _on_eskere():
	var ev := InputEventAction.new();
	ev.action = "right";
	ev.pressed = false;
	Input.parse_input_event(ev);

func _on_level_changed(needs_minimap: bool, new_tileset: TileSet, new_gridmap: GridMap, spawn_point: Vector3):
	translation = spawn_point;

func _physics_process(delta):
	#Rootin tootin shootin logic
	if Input.is_action_just_pressed("shoot"): gun.shoot(false);
	elif Input.is_action_pressed("shoot"): gun.shoot(true);

	# Movement logic
	var velocity := Vector3();
	if Input.is_action_pressed("left"): velocity.x -= 1;
	if Input.is_action_pressed("right"): velocity.x += 1;
	if Input.is_action_pressed("up"): velocity.z -= 1;
	if Input.is_action_pressed("down"): velocity.z += 1;
	#if Input.is_action_pressed("fly"): velocity.y += 2;
	#if Input.is_action_just_pressed("pause"):
	#	print("stop");

	if velocity == Vector3.ZERO:
		trail.set_emitting(false);
		anim.stop(false);
	else:
		trail.set_emitting(true);
		# This comment in memoriam of all the time wasted on figuring out this god forsaken piece of math
		var apollyon := fposmod(Vector2(velocity.z, velocity.x).angle() + PI - rotation.y + PI, 2 * PI);
		if apollyon < PI*1/4 || apollyon > PI*7/4: anim.play("Walk_Forward");
		elif apollyon < PI*1/2: anim.play("Walk_Leftward");
		elif apollyon > PI*3/2: anim.play("Walk_Rightward");
		elif apollyon < PI*3/4: anim.play_backwards("Walk_Rightward");
		elif apollyon > PI*5/4: anim.play_backwards("Walk_Leftward");
		else: anim.play_backwards("Walk_Forward");

	velocity.y -= 1;
	accel *= 0.9;
	move_and_slide(velocity.normalized() * speed + accel);

func _input(event):
	# Rotate with mouse
	if event is InputEventMouseMotion:
		var mouse := viewport.get_mouse_position();
		var origin := camera.project_ray_origin(mouse);
		var normal := camera.project_ray_normal(mouse);
		var intersect = Plane(Vector3.UP, 8.595).intersects_ray(origin, normal);
		intersect = Vector2(intersect.x, intersect.z);
		rotation.y = -Vector2(translation.x, translation.z).angle_to_point(intersect) - PI / 2;

func _on_hitbox_body_entered(body):
	._on_hitbox_body_entered(body);
	EVENTS.emit_signal("marano_life_changed", life);
	if life <= 0:
		EVENTS.emit_signal("marano_died");
		get_tree().paused = true;

func _on_capone_punch(direction: Vector3):
	accel += direction * 100;
	life -= 2;
	life = max(life, 0);
	EVENTS.emit_signal("marano_life_changed", life);
	if life <= 0:
		EVENTS.emit_signal("marano_died");
		get_tree().paused = true;

extends Node

signal level_changed(needs_minimap, new_tileset, new_gridmap);
signal level_ended();

signal marano_life_changed(life);
signal marano_died();

signal boss_spawned(life, name);
signal boss_life_changed(life);
signal boss_died();

signal capone_punch(direction);

signal fade_in();
signal fade_in_ended();
signal fade_out();

signal roll_credits();
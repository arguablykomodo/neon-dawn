extends Level

var block_sizes := [1, 2, 3, 4];
var map_size := 16;
var blocks := [];
var drone := preload("res://entities/enemies/drone/drone.tscn");
var drone_count := 0;

func _ready():
	needs_minimap = true;
	tileset = preload("res://levels/city/tileset.tres");
	gridmap = $GridMap;
	generate_map();
	EVENTS.connect("boss_died", self, "_on_boss_died");

# Does rectangle intersection to check if a block fits in the current position
func fits(x: int, z: int, w: int, d: int) -> bool:
	for block in blocks:
		if x < block.x + block.w and x + w > block.x and z < block.z + block.d and z + d > block.z:
			return false;
	return true;

# Places a new block on the map
func place():
	var w: int = UTILS.pick(block_sizes) + 1;
	var d: int = UTILS.pick(block_sizes) + 1;
	for x in range(map_size - w + 2):
		for z in range(map_size - d + 2):
			if fits(x, z, w, d):
				blocks.append({"x": x, "z": z, "w": w, "d": d});
				for local_x in range(w - 1):
					for local_z in range(d - 1):
						gridmap.set_cell_item(x + local_x, 0, z + local_z, 0);
				return;

# Checks if the map has been completely filled with blocks
func is_full() -> bool:
	var filled := 0;
	for block in blocks:
		filled += block.w * block.d;
	return filled >= map_size * map_size;

func generate_map():
	randomize();
	
	# Generate blocks
	for i in range(map_size * map_size):
		place();
		if is_full():
			break;
	
	# Generate boundary
	for i in range(map_size):
		gridmap.set_cell_item(-1, 0, i, 0);
		gridmap.set_cell_item(map_size, 0, i, 0);
		gridmap.set_cell_item(i, 0, -1, 0);
		gridmap.set_cell_item(i, 0, map_size, 0);
	
	var spawn_points = [];
	
	# Generate streets
	for z in range(map_size):
		for x in range(map_size):
			if gridmap.get_cell_item(x, 0, z) == -1:
				var encoding := 0;
				var openings := 0;
				if gridmap.get_cell_item(x    , 0, z - 1) != 0:
					encoding += 1;
					openings += 1;
				if gridmap.get_cell_item(x + 1, 0, z    ) != 0:
					encoding += 2;
					openings += 1;
				if gridmap.get_cell_item(x    , 0, z + 1) != 0:
					encoding += 4;
					openings += 1;
				if gridmap.get_cell_item(x - 1, 0, z    ) != 0:
					encoding += 8;
					openings += 1;
				gridmap.set_cell_item(x, 0, z, 1 + encoding);
				
				spawn_points.append(Vector3(x * 16 + 8, 0, z * 16 + 8));
				
				# Randomly spawn drones on intersections
				if openings > 2 && randf() > 0.75:
					drone_count += 1;
					var new_drone: KinematicBody = drone.instance();
					new_drone.translation.x = x * 16 + 8;
					new_drone.translation.y = 12;
					new_drone.translation.z = z * 16 + 8;
					add_child(new_drone);
	gridmap.make_baked_meshes();
	
	# Position player
	var player_target = Vector3(0, 0, 0);
	var player_spawn = spawn_points[0];
	for spawn in spawn_points:
		if spawn.distance_to(player_target) < player_spawn.distance_to(player_target):
			player_spawn = spawn
	
	spawn_point = player_spawn;
	
	# Position capone
	var capone_target = Vector3(16 * map_size, 0, 16 * map_size);
	var capone_spawn = spawn_points[0];
	for spawn in spawn_points:
		if spawn.distance_to(capone_target) < capone_spawn.distance_to(capone_target):
			capone_spawn = spawn
	
	var capone = load("res://entities/bosses/capone/capone.tscn").instance();
	capone.translation = capone_spawn;
	add_child(capone);

func _on_boss_died():
	end_level();

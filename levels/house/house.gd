extends Level

func _ready():
	needs_minimap = false;

func _on_door_body_entered(body: KinematicBody):
	if body.is_in_group("marano"):
		end_level();

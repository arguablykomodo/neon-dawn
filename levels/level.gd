extends Node
class_name Level

var needs_minimap: bool;
var tileset: TileSet;
var gridmap: GridMap;
var spawn_point: Vector3;

func end_level():
	EVENTS.emit_signal("level_ended");

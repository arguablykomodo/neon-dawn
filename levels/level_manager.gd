extends Node

var levels: Array = UTILS.parse_json_file("res://levels/levels.json");
var level_index := 0;

func _ready():
	EVENTS.connect("level_ended", self, "_on_level_ended");
	EVENTS.connect("fade_in_ended", self, "_on_fade_in_ended");
	load_level();

func load_level():
	var scene := load("res://levels/{name}/{name}.tscn".format({"name": levels[level_index]}));
	var level: Level = scene.instance();
	if get_child_count() > 0: 
		get_child(0).queue_free();
	add_child(level);
	EVENTS.emit_signal("level_changed", level.needs_minimap, level.tileset, level.gridmap, level.spawn_point);

func _on_fade_in_ended():
	level_index += 1;
	load_level();

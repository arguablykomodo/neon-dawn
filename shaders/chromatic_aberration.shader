shader_type canvas_item;

uniform vec2 screen_size = vec2(512, 288);
uniform int steps = 16;

vec3 distortion(vec2 offset, sampler2D tex, vec2 uv) {
	vec3 col = vec3(0);
	col.r += texture(tex, uv - offset).r;
	col.g += texture(tex, uv).g;
	col.b += texture(tex, uv + offset).b;
	return col;
}

void fragment() {
	vec2 direction = pow(UV * 2.0 - 1.0, vec2(2.0));
	vec3 col = vec3(0);
	for (int i = 0; i < steps; i++) {
		col += distortion(direction * (1.0 / screen_size) * float(i), TEXTURE, UV);
	}
	col /= float(steps);
	COLOR.rgb = col;
}

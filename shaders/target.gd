extends Sprite

onready var viewport := get_tree().get_root();

func _input(event):
	if event is InputEventMouseMotion:
		var mouse := viewport.get_mouse_position();
		position = mouse;

extends ColorRect

func _ready():
	hide();
	EVENTS.connect("marano_died", self, "_on_died");

func _on_died():
	show();

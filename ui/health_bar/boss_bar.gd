extends HBoxContainer

onready var bar: TextureProgress = $Bar;
onready var label: Label = $Bar/Label;

func _ready():
	hide();
	EVENTS.connect("boss_spawned", self, "_on_boss_spawned");
	EVENTS.connect("boss_life_changed", self, "_on_life_changed");
	EVENTS.connect("boss_died", self, "_on_boss_died");

func _on_boss_spawned(life: int, name: String):
	label.text = name;
	bar.max_value = life;
	show();
	
func _on_life_changed(life: int):
	bar.value = life;

func _on_boss_died():
	hide();

extends HBoxContainer

onready var bar: TextureProgress = $Bar;
onready var label: Label = $Bar/Label;

func _ready():
	EVENTS.connect("marano_life_changed", self, "_on_life_changed");

func _on_life_changed(life: int):
	bar.value = life;
	label.text = str(life) + "/10";

extends ColorRect

onready var tween: Tween = $Tween;

var fading_in := true;

func _ready():
	EVENTS.connect("level_changed", self, "_on_fade_out");
	EVENTS.connect("level_ended", self, "_on_fade_in");

func _on_fade_in():
	fading_in = true;
	tween.interpolate_property(self, "color", Color(0, 0, 0, 0), Color.black, 1, Tween.TRANS_CUBIC, Tween.EASE_IN);
	tween.start();

func _on_fade_out(needs_minimap: bool, new_tileset: TileSet, new_gridmap: GridMap, spawn_point: Vector3):
	fading_in = false;
	tween.interpolate_property(self, "color", Color.black, Color(0, 0, 0, 0), 1, Tween.TRANS_CUBIC, Tween.EASE_IN);
	tween.start();

func _on_tween_completed(object, key):
	if fading_in:
		EVENTS.emit_signal("fade_in_ended");

extends Control

onready var text: RichTextLabel = $RichTextLabel;
onready var bg: ColorRect = $ColorRect;
onready var tween: Tween = $Tween;
onready var skip: Label = $Label;

func _ready():
	tween.interpolate_property(skip, "margin_bottom", 0, 10, 2, Tween.TRANS_SINE, Tween.EASE_IN_OUT, 2);
	tween.interpolate_property(text, "margin_top", 318, -190, 40, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT);
	tween.interpolate_property(bg, "color",
		Color(0.1, 0, 0.1, 1),
		Color(0, 0, 0, 0),
	1, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT, 40);
	tween.start();

func _input(event: InputEvent):
	if event.is_action_pressed("fly"):
		tween.stop_all();
		bg.color = Color(0, 0, 0, 0);
		text.margin_top = -190;
		skip.margin_bottom = 10;
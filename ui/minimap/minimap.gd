extends NinePatchRect

onready var timer: Timer = $ViewportContainer/Viewport/TileMap/Timer;
onready var tilemap: TileMap = $ViewportContainer/Viewport/TileMap;

func _ready():
	EVENTS.connect("level_changed", self, "_on_level_changed");

func _on_level_changed(needs_minimap: bool, new_tileset: TileSet, new_gridmap: GridMap, spawn_point: Vector3):
	if !needs_minimap:
		timer.stop();
		hide();
	else:
		tilemap.clear();
		tilemap.tile_set = new_tileset;
		tilemap.gridmap = new_gridmap;
		timer.start();
		show();

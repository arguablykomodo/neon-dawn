extends Sprite

onready var marano: KinematicBody = get_tree().get_nodes_in_group("marano")[0];

func _process(delta):
	position = Vector2(marano.translation.x / 2, marano.translation.z / 2);

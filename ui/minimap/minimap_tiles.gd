extends TileMap

onready var marano: KinematicBody = get_tree().get_nodes_in_group("marano")[0];
var gridmap: GridMap;

func _on_timer_timeout():
	for y in range(-1, 2):
		for x in range(-1, 2):
			var x_tile := floor(marano.translation.x / 16) + x;
			var y_tile := floor(marano.translation.z / 16) + y;
			set_cell(x_tile, y_tile, gridmap.get_cell_item(x_tile, 0, y_tile));

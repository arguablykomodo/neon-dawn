extends Control

onready var winbg: ColorRect = $Winbg;
onready var wintext: RichTextLabel = $Wintext;
onready var skiplabel: RichTextLabel = $Label;

func _ready():
	winbg.hide();
	wintext.hide();
	skiplabel.hide();
	EVENTS.connect("roll_credits", self, "_on_roll_credits");

func _on_roll_credits():
	winbg.show();
	wintext.show();
	skiplabel.show();

func _input(event: InputEvent):
	if event.is_action_pressed("quit"):
		get_tree().quit();

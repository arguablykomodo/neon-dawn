extends Node

func parse_json_file(url: String):
	var file := File.new();
	file.open(url, file.READ);
	var text := file.get_as_text();
	var json = parse_json(text);
	file.close();
	return json;

func pick(array: Array):
	return array[randi() % array.size()];

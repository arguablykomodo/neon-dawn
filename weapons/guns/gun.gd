extends Spatial
class_name Gun

export (String) var title;
export (String) var description;
export (String, FILE) var projectile;
export (int) var damage;
export (float) var fire_rate;
export (bool) var auto;
export (bool) var is_friendly;

onready var projectile_origin: Spatial = $Origin;
onready var projectile_scene := load(projectile);
onready var timer := Timer.new();

var can_shoot := true;

func _ready():
	timer.wait_time = fire_rate;
	timer.one_shot = true;
	timer.connect("timeout", self, "_on_timeout");
	add_child(timer);

func _on_timeout():
	can_shoot = true;
	pass

func shoot(is_auto: bool):
	if (auto == is_auto) and can_shoot:
		var new_projectile: Projectile = projectile_scene.instance();
		new_projectile.translation = projectile_origin.global_transform.origin;
		new_projectile.rotation.y = projectile_origin.global_transform.basis.get_euler().y;
		new_projectile.is_friendly = is_friendly;
		new_projectile.damage = damage;
		get_tree().root.add_child(new_projectile);
		can_shoot = false;
		timer.start();
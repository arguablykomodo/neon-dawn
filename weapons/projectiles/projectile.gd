extends KinematicBody
class_name Projectile

export (int) var speed;
export (float) var timeout;

var damage: int;
var is_friendly: bool;
var direction := Vector3(0, 0, 0);

func _ready():
	direction.x = cos(-rotation.y + PI / 2);
	direction.z = sin(-rotation.y + PI / 2);
	
	var timer := Timer.new();
	timer.set_wait_time(timeout);
	timer.connect("timeout", self, "_on_timeout");
	timer.start();
	add_child(timer);

func _physics_process(delta):
	move_and_slide(direction * speed);
	for i in range(get_slide_count()):
		var collision = get_slide_collision(i);
		if typeof(collision.collider) == typeof(StaticBody):
			queue_free();

func _on_timeout():
	queue_free();
